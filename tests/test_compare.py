import compare
import nose.tools as noset
import os
import shutil
import io
import tempfile
import contextlib
import difflib

FakeFile = type('_FakeFile', (io.StringIO,), {'name': None})

@contextlib.contextmanager
def temp_dir():
    tmp = tempfile.mkdtemp()
    cur = os.getcwd()
    os.chdir(tmp)
    yield
    os.chdir(cur)
    shutil.rmtree(tmp)


class TestCompare(object):
    def setup(self):
        self.replica_prev = FakeFile(u'\n'.join([
            'repl_a_1',
            'repl_a_2',
            'repl_a_3',
            'repl_a_4',
            'repl_a_5',
            'repl_a_6',
        ]))
        self.replica_next = FakeFile(u'\n'.join([
            'repl_a_1',
            'repl_a_2',
            'repl_a_3',
            'repl_b_4',
            'repl_b_5',
            'repl_b_6',
        ]))
        self.storage = FakeFile(u'\n'.join([
            'repl_a_2',
            'repl_a_3',
            'repl_b_4',
        ]))
        
        self.unsorted_storage = FakeFile(u'\n'.join([
            'repl_b_4',
            'repl_a_3',
            'repl_a_2',
        ]))

        self.replica_prev.name = 'replica_prev'
        self.replica_next.name = 'replica_next'
        self.storage.name = 'storage'
        self.unsorted_storage.name = 'unsorted_storage'

        self.intersection = [
            'repl_a_1',
            'repl_a_2',
            'repl_a_3',
        ]

    def test_intersection(self):
        with temp_dir():
            i = compare.intersection(self.replica_prev, self.replica_next)
            ilist = i.readlines()

        noset.ok_(all(expected == calculated.strip()
            for expected, calculated in zip(self.intersection, ilist)))

    def test_cut_name(self):
        noset.eq_(7, int(compare.cut_name('\t'.join(map(str, range(1, 10))))))

    def test_sort_dump(self):
        with temp_dir():
            srtd = compare.sort_dump(self.unsorted_storage).readlines()

        noset.eq_(3, len(srtd))
        noset.ok_(all(expected.strip() == calculated.strip()
            for expected, calculated in zip(self.storage.readlines(), srtd)))

