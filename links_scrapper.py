#!/usr/bin/env python2
import bs4
import requests
import sys

soup = bs4.BeautifulSoup(requests.get(sys.argv[1]).text, 'html.parser')

for a in soup.find_all('a'):
    link = a.get('href')
    if not link.startswith('http://') and not link.startswith('https://'):
        print('{0}/{1}'.format(sys.argv[1], link))
    else:
        print(link)
