#!/usr/bin/env python
import itertools
import argparse
from datetime import datetime
#from rucio.rse.protocols.gfal import Default
import gfal



def chunks(iterable, size=1024):
    it = iter(iterable)
    chunk = list(itertools.islice(it, size))
    while chunk:
        yield chunk
        chunk = list(itertools.islice(it, size))


def parse_paths(url, paths):
    for path in paths:
        yield '/'.join((url, path.strip()))


if __name__ == '__main__':
    import sys
    # srm_proto = Default({}, {'deterministic': True})
    # srm_proto.connect()
    parser = argparse.ArgumentParser(
        description='Verify if the input paths exists in any of the given DDM Endpoints'
    )
    parser.add_argument('--bulk', type=int, default=1024, help='Number of paths sent to the server in each request')
    args = parser.parse_args()


    for urls in chunks(sys.stdin.xreadlines(), size=args.bulk):
        status, internal, desc = gfal.gfal_init({'surls': urls})
        assert status == 0, 'gfal_init returned: {0}, "{1}"'.format(status, desc)

        status, internal, desc = gfal.gfal_deletesurls(internal)
        assert status == 0, 'gfal_deletesurls returned: {0}, "{1}"'.format(status, desc)

        status, internal, desc = gfal.gfal_get_results(internal)
        assert status == len(urls), 'Every URL must have a status'
        for entry in desc:
            print('STATUS{0},{1}'.format(entry['status'], entry['surl']))
    
#    for url in sys.stdin.xreadlines():
#        try:
#            srm_proto.delete(url)
#        except Exception as e:
#            print('FAILED,' + str(url) + ',' + e.__class__.__name__ + ':' + e.message)
#        else:
#            print('SUCCESS,' + str(url) + ',')

#    for urls in chunks(sys.stdin.xreadlines(), size=args.bulk):
#        try:
#            srm_proto.delete(urls)
#        except Exception as e:
#            print('FAILED,' + str(urls) + ',' + e.__class__.__name__ + ':' + e.message)
#        else:
#            print('SUCCESS,' + str(urls) + ',')

        
#    for urls in chunks(sys.stdin.xreadlines(), size=args.bulk):
#        for url in urls:
#            print url
#            gfal.gfal_unlink(url)

#    for urls in chunks(sys.stdin.xreadlines(), size=args.bulk):
#        status, internal, desc = gfal.gfal_init({'surls': urls})
#        assert status == 0, 'gfal_init returned: {0}, "{1}"'.format(status, desc)
#
#        status, internal, desc = gfal.gfal_deletesurls(internal)
#        assert status == 0, 'gfal_deletesurls returned: {0}, "{1}"'.format(status, desc)
#
#        status, internal, desc = gfal.gfal_get_results(internal)
#        assert status == len(urls), 'Every URL must have a status'
#        for entry in desc:
#            print('STATUS{0},{1}'.format(entry['status'], entry['surl']))
