#!/usr/bin/env python
import gfal
import rucio.client
import itertools
import argparse

def chunks(iterable, size=1024):
    it = iter(iterable)
    chunk = list(itertools.islice(it, size))
    while chunk:
        yield chunk
        chunk = list(itertools.islice(it, size))


def exists(urls):
    status, internal, desc = gfal.gfal_init({'surls': urls})
    assert status == 0, 'gfal_init returned: {0}, "{1}"'.format(status, desc)
    gfal.gfal_ls(internal)
    status, internal, desc = gfal.gfal_get_results(internal)
    assert status == len(urls), 'gfal_get_results returned: {0}, "{1}"'.format(status, desc)
    return itertools.izip(urls, desc)

def url_for_endpoint(endpoint):
    # FIXME: Assuming only SRM
    info = rucio.client.RSEClient().get_rse(endpoint)
    srm_proto = [proto for proto in info['protocols'] if proto.get('scheme', None) == 'srm'][0]
    url = '{0}://{1}:{2}/{3}'.format('srm', srm_proto['hostname'], srm_proto['port'], srm_proto['prefix'].strip('/'))
    return url

def parse_paths(url, paths):
    sam = '/'.join(url.split('/')[:-1])
    for path in paths:
        if path.startswith('SAM/') or path.startswith('panda/destDB/'):
            yield '/'.join((sam, path)).strip()
        else:
            yield '/'.join((url, path)).strip()

   

if __name__ == '__main__':
    import sys
    parser = argparse.ArgumentParser(
        description='Verify if the input paths exists in any of the given DDM Endpoints'
    )
    parser.add_argument('endpoint', metavar='DDM_ENDPOINT')
    parser.add_argument('--bulk', type=int, default=1024, help='Number of paths sent to the server in each request')
    args = parser.parse_args()
    prefix = url_for_endpoint(args.endpoint)

    for paths in chunks(sys.stdin.xreadlines(), size=args.bulk):
        paths_status = exists(list(parse_paths(prefix, paths)))
        for url, status in paths_status:
            print(
                'STATUS{0},{1},{2}'.format(
                    status['status'],
                    url,
                    status['stat'][6] if 'stat' in status.keys() else None
                )
            )
