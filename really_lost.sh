#!/bin/sh
set -e

replicas(){
    rucio list-file-replicas "$1:$2" | tail -n-2 | grep -oE '\w+:\s.+$'
}

cat $1 | while read line; do
    line=$(echo "$line" | sed -r 's|^/||')
    namespace=$(echo "$line" | cut -d/ -f1)
    filename=$(basename "$line")

    if [ -z "$line" ]; then
        continue
    fi


    while read replica; do
        rse=$(echo $replica | cut -d: -f1)
        path=$(echo $replica | cut -d: -f2-)
        status=0
        gfal-ls $path > /dev/null || status=$?
        if [ $status -eq 2 ]; then
            echo LOST,$rse,$path
        elif [ $status -eq 2 ]; then
            echo ERR$status,$rse,$path
        else
            echo STATUS$status,$rse,$path
        fi
    done <<< "$(replicas "$namespace" "$filename")"

    #echo "Checking $namespace:$filename" 1>&2
    #if have_replicas "$namespace" "$filename"; then
    #    echo "1,$namespace:$filename"
    #else
    #    echo "0,$namespace:$filename"
    #fi
done



@
