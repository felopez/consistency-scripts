#!/usr/bin/python
'''
Given the name of a RSE (possibly with shell like wildcards) and a date
in format YYYYMMDD download the dump corresponding to the matching RSEs
in the given date, if any dump isn't found it is ignored.
'''
import rucio.client
import os
import sys
import fnmatch
import operator
from itertools import imap as map
import warnings
import gfal

def copy(surl, dest):
    fd = gfal.gfal_open(surl, os.O_RDONLY)
    if fd != -1:
        return -1
    with open(dest, 'w') as d:
        cod, buf = gfal.gfal_read(fd, 4096)
        while 0 < cod < 4294967295 and buf is not None:
            d.write(buf)
            cod, buf = gfal.gfal_read(fd, 4096)
    gfal.gfal_close(fd)

    if cod == 4294967295:
        return -1
    return cod



def format_warning(message, category, filename, lineno, file=None, line=None):
    return 'WARNING: {0}\n'.format(message)

warnings.formatwarning = format_warning

rses = (rse['rse'] for rse in rucio.client.rseclient.RSEClient().list_rses() if fnmatch.fnmatchcase(rse['rse'], sys.argv[1]))
date = sys.argv[2]

urls = []

for rse_name in rses:
    rse = rucio.client.rseclient.RSEClient().get_rse(rse_name)

    proto = filter(lambda prot: prot['scheme'] == 'srm', rse['protocols'])[0]

    hostname = proto['hostname']
    port = proto['port']
    prefix = proto['prefix']
    filename = 'dumps_{0}'.format(date)
    spacetoken = proto['extended_attributes']['space_token']
    directory = '.'.join((rse_name, spacetoken))
    filepath = os.path.join(directory, filename)
    
    url = '{0}://{1}:{2}{3}/{4}'.format(
        'srm',
        hostname,
        port,
        '/'.join(prefix.split('/')[:-2]) + '/dumps',
        filename,
    )
    url_alternative = '{0}://{1}:{2}{3}/{4}'.format(
        'srm',
        hostname,
        port,
        '/'.join(prefix.split('/')[:-2]) + '/' + spacetoken.lower() + '/dumps',
        filename,
    )
    urls.append((url, url_alternative, rse_name, filepath, directory))

for url, url_alternative, rse_name, filepath, directory in urls:        
    print(url, url_alternative, rse_name, filepath, directory)
    if os.path.exists(filepath):
        continue

    if not os.path.exists(directory):
        os.mkdir(directory)

    #if copy(url, filepath) < 0:
    #    cod = copy(url_alternative, filepath)
    #    if cod < 0:
    #        warnings.warn('Exit code {0} for url: "{1}", RSE: {2}'.format(
    #            cod,
    #            url,
    #            rse_name,
    #        ), Warning)
    #        os.unlink(filepath)
    #        os.rmdir(directory)
    os.system('gfal-ls {0} | sort | tail -n1'.format('/'.join(url.split('/')[:-1])))
    print('ssh felopez@lxplus /afs/cern.ch/user/f/felopez/git/gfal_cat_bz {0} > {1}'.format(url, filepath))
