#!/usr/bin/python
'''
Given the name of a RSE (possibly with shell like wildcards) and 
part of the path where the file is stored, the script prints
for each matching prefix:
    rse_name, spacetoken, url, prefix 
'''
import rucio.client
import os
import sys
import fnmatch
import operator
from itertools import imap as map
import gfal
import lcg_util
import argparse

units = (
    ('B', 1),
    ('KiB', 2**10),
    ('MiB', 2**20),
    ('GiB', 2**30),
    ('TiB', 2**40),
)

parser = argparse.ArgumentParser()
parser.add_argument('rse_pattern')
parser.add_argument('--unit', choices=(unit[0] for unit in units), default='TiB')
args = parser.parse_args()


rses = (rse['rse'] for rse in rucio.client.rseclient.RSEClient().list_rses() if fnmatch.fnmatchcase(rse['rse'], sys.argv[1]))

def rucio_info(rses):
    for rse_name in rses:
        rse = rucio.client.rseclient.RSEClient().get_rse(rse_name)

        proto = filter(lambda prot: prot['scheme'] == 'srm', rse['protocols'])[0]

        hostname = proto['hostname']
        port = proto['port']
        prefix = proto['prefix']
        spacetoken = proto['extended_attributes']['space_token']

        usage_list = list(rucio.client.rseclient.RSEClient().get_rse_usage(rse_name))


        usage_rucio = [entry for entry in usage_list if entry['source'] == 'rucio'][0]['used']
        usage_srm = [entry for entry in usage_list if entry['source'] == 'srm']
        if not usage_srm:
            usage_srm = -1
        else:
            usage_srm = usage_srm[0]['used']

        yield (hostname, rse_name, spacetoken, usage_rucio, usage_srm)

def convert_unit(number, unit=None):
    if unit is None:
        unit = [(name, divisor) for name, divisor in units if name == args.unit][0]
    return '{0:.2f}{1}'.format(float(number) / unit[1], unit[0])
    

# Force to perform all the Rucio queries at one, as mixing rucio calls
# with lcg_util calls cause a SSL error with requests
rses_data = list(rucio_info(rses))

print('"rse_name", "spacetoken", "usage_rucio", "usage_srm", "usage_lcg_stmd", "usage_rucio - usage_lcg_stmd"')
for rse_data in rses_data:
    hostname, rse_name, spacetoken, usage_rucio, usage_srm = rse_data
    try:
        usage_lcg_stmd = lcg_util.lcg_stmd(spacetoken, hostname, False, 0)[1][0]
        usage_lcg_stmd = usage_lcg_stmd['totalsize'] - usage_lcg_stmd['unusedsize']
    except TypeError:
        usage_lcg_stmd = -1


    sys.stdout.write(','.join([
        rse_name,
        spacetoken,
        convert_unit(usage_rucio),
        convert_unit(usage_srm),
        convert_unit(usage_lcg_stmd),
        convert_unit((usage_rucio - usage_lcg_stmd) if usage_lcg_stmd > 0 else -1),
    ]) + '\n')
