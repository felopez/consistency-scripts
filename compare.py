#!/usr/bin/env python

import sys
import os



def noop(s):
    return s.strip().lstrip('/')

def cut_name(s):
    return s.split('\t')[6].strip().lstrip('/')

def sort_dump(dump, cut=noop):
    sorted_name = '_'.join((dump.name, 'sorted'))
    if not os.path.exists(sorted_name):
        with open(sorted_name, 'w') as sorted_f:
            sorted_f.writelines(sorted(cut(row) + '\n' for row in dump.readlines()))
    return open(sorted_name, 'r')

def intersection(sorted_f1, sorted_f2):
    intersection_name = '_'.join(('merge', sorted_f1.name, sorted_f2.name))
    if not os.path.exists(intersection_name):
        with open(intersection_name, 'w') as intersection:
            intersection.writelines(sorted(set(sorted_f1.readlines()).intersection(set(sorted_f2.readlines())), key=str.lower))
    return open(intersection_name, 'r')

def difference(sorted_f1, sorted_f2):
    '''Returns an iterable which has the result of the difference operator
    (as in sets): sorted_f1 - sorted_f2'''
    s1 = sorted_f1.readlines()
    s2 = sorted_f2.readlines()
    assert s1[0].endswith('\n')
    assert s2[0].endswith('\n')

    return set(s1).difference(set(s2))
    #return set(sorted_f1.readlines()).difference(set(sorted_f2.readlines())):


if __name__ == '__main__':
    prev_rucio_dump = open(sys.argv[1])
    storage_dump = open(sys.argv[2])
    next_rucio_dump = open(sys.argv[3])

    prev_rucio_dump_sorted = sort_dump(prev_rucio_dump, cut_name)
    storage_dump_sorted = sort_dump(storage_dump)
    next_rucio_dump_sorted = sort_dump(next_rucio_dump, cut_name)

    prev_rucio_dump.close()
    storage_dump.close()
    next_rucio_dump.close()

    intersection_f = intersection(prev_rucio_dump_sorted, next_rucio_dump_sorted)

    os.system('comm -23 ' + storage_dump_sorted.name + ' ' + intersection_f.name)
