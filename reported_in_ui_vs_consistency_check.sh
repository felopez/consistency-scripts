#!/bin/sh

cat "$2" | python json2list.py | while read line; do
    path=$(echo $line | cut -d\; -f2)
    if grep "$path" "$1" > /dev/null; then
        found=$(grep "$path" "$1")
        if [ -z "$(echo $found | grep ^LOST)" ]; then
            echo "BAD: $found"
#        else
#            echo "GOOD: $found"
        fi
    else
        echo "NOT_FOUND: $line"
    fi
done
