#!/usr/bin/env python
import itertools
import argparse
from datetime import datetime
from rucio.rse.protocols.gfal import Default
from rucio.rse import rsemanager as rsemgr



def chunks(iterable, size=1024):
    it = iter(iterable)
    chunk = list(itertools.islice(it, size))
    while chunk:
        yield chunk
        chunk = list(itertools.islice(it, size))


def parse_paths(url, paths):
    for path in paths:
        yield '/'.join((url, path.strip()))


if __name__ == '__main__':
    import sys
    #srm_proto = Default({}, {'deterministic': True})
    #srm_proto.connect()
    parser = argparse.ArgumentParser(
        description='Verify if the input paths exists in any of the given DDM Endpoints'
    )
    parser.add_argument('--bulk', type=int, default=1024, help='Number of paths sent to the server in each request')
    parser.add_argument('--rse', help='RSE name')
    args = parser.parse_args()

    rse_info = rsemgr.get_rse_info(args.rse)
    proto = rsemgr.create_protocol(rse_info, 'delete', scheme='srm')
    for url in sys.stdin.xreadlines():
        url = url.strip()
        try:
            proto.delete(url)
        except Exception as e:
            print('FAILED,' + str(url) + ',' + str(e).strip())
        else:
            print('SUCCESS,' + str(url) + ',')

#    for urls in chunks(sys.stdin.xreadlines(), size=args.bulk):
#        try:
#            proto.delete([url.strip() for url in urls])
#        except Exception as e:
#            print('FAILED,' + str(urls) + ',' + str(e))
#        else:
#            print('SUCCESS,' + str(urls) + ',')

    # sample = "srm://grid-cert-03.roma1.infn.it:8446/dpm/roma1.infn.it/home/atlas/atlasdatadisk/rucio/data11_7TeV/05/37/data11_7TeV.00187815.physics_Muons.merge.AOD.f396_m945._lb0071-lb0072._0001.1"
    # sample = "srm://grid-cert-03.roma1.infn.it:8446/srm/managerv2?SFN=/dpm/roma1.infn.it/home/atlas/atlasdatadisk/rucio/data12_8TeV/7f/5a/data12_8TeV.00203335.physics_JetTauEtmiss.merge.AOD.f446_m1148._lb0063._0001.1"
    # sample = "srm://grid-cert-03.roma1.infn.it:8446/srm/managerv2?SFN=/dpm/roma1.infn.it/home/atlas/atlasdatadisk/rucio/data12_8TeV/7f/61/data12_8TeV.00203258.physics_JetTauEtmiss.merge.AOD.f444_m1143._lb0118._0001.1"
    # try:
    #    proto.delete(sample)
    #except Exception as e:
    #    import pdb; pdb.set_trace()

#    for url in sys.stdin.xreadlines():
#        url = url.strip()
#        try:
#            proto.delete(url)
#        except Exception as e:
#            print('FAILED,' + str(url) + ',' + e.__class__.__name__ + ':' + e.message)
#        else:
#            print('SUCCESS,' + str(url) + ',')

#    for urls in chunks(sys.stdin.xreadlines(), size=args.bulk):
#        try:
#            srm_proto.delete(urls)
#        except Exception as e:
#            print('FAILED,' + str(urls) + ',' + e.__class__.__name__ + ':' + e.message)
#        else:
#            print('SUCCESS,' + str(urls) + ',')

#    for url in sys.stdin.xreadlines():
#        try:
#            srm_proto.delete(url)
#        except Exception as e:
#            print('FAILED,' + str(url) + ',' + e.__class__.__name__ + ':' + e.message)
#        else:
#            print('SUCCESS,' + str(url) + ',')


        
#    for urls in chunks(sys.stdin.xreadlines(), size=args.bulk):
#        for url in urls:
#            print url
#            gfal.gfal_unlink(url)

#    for urls in chunks(sys.stdin.xreadlines(), size=args.bulk):
#        status, internal, desc = gfal.gfal_init({'surls': urls})
#        assert status == 0, 'gfal_init returned: {0}, "{1}"'.format(status, desc)
#
#        status, internal, desc = gfal.gfal_deletesurls(internal)
#        assert status == 0, 'gfal_deletesurls returned: {0}, "{1}"'.format(status, desc)
#
#        status, internal, desc = gfal.gfal_get_results(internal)
#        assert status == len(urls), 'Every URL must have a status'
#        for entry in desc:
#            print('STATUS{0},{1}'.format(entry['status'], entry['surl']))
