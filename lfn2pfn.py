#!/usr/bin/env python
import sys
import hashlib

for line in sys.stdin:
    line = line.strip()
    scope, filename = line.split(':')
    first_part = scope.replace('.', '/')
    md5 = hashlib.md5(line).hexdigest()
    second_part = '{0}/{1}'.format(md5[0:2], md5[2:4])
    third_part = filename

    print('/'.join((first_part, second_part, third_part)))

