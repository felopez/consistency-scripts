#!/usr/bin/env python2
import argparse
import cPickle
import json
import operator
import os
import pprint
import requests
import sys
from functools import partial

fields = {
    'is_tape': bool,
    'domain': unicode,
    'se_flavour': unicode,
    'phys_groups': list,
    'site': unicode,
    'rc_site': unicode,
    'is_volatile': bool,
    'cloud': unicode,
    'site_state': unicode,
    'rc_site_state': unicode,
    'servedrestfts': dict,
    'quotas': dict,
    'is_cache': bool,
    'state': unicode,
    'rc': unicode,
    'type': unicode,
    'is_pledged': bool,
    'aprotocols': dict,
    'space_method': unicode,
    'last_modified': unicode,
    'servedfts': dict,
    'datapolicies': list,
    'protocols': dict,
    'permissions': dict,
    'endpoint': unicode,
    'rc_country': unicode,
    'name': unicode,
    'is_rucio': bool,
    'country': unicode,
    'ddmgroups': list,
    'is_deterministic': bool,
    'is_mkdir': bool,
    'token': unicode,
    'tier_level': int,
    'se': unicode,
    'servedlfc': unicode,
}


def parse_args(args):
    parser = argparse.ArgumentParser(prog=args[0])
    parser.add_argument('--pretty', action='store_true')
    parser.add_argument('-f', '--file', default=None, metavar='FILENAME')
    for field, type_ in fields.items():
        parser.add_argument('--' + field.replace('_', '-'), action='append_const', dest='show_fields', const=field)

    for field, type_ in fields.items():
        if type_ in (unicode, str, int, float, bool):
            parser.add_argument('--filter-' + field)

    return parser.parse_args(args[1:])

def build_filter_funcs(args):
    filter_exprs = [(argument[7:], value) for argument, value in args._get_kwargs() if value is not None and argument.startswith('filter_')]
    filter_funcs = []
    for field, value in filter_exprs:
        filter_funcs.append(lambda endpoint: endpoint[field] == value)
        filter_funcs[-1].generated_as = partial(str.format, 'lambda endpoint: endpoint[{0}] == {1}', field, value)
    return filter_funcs


priorities = {
    'name': -3,
    'se': -2,
    'endpoint': 1,
}
def tweak_priorities(key):
    return priorities.get(key, key)

def match_all(filter_funcs, endpoint):
    if not filter_funcs:
        return True
    return all(func(endpoint) for func in filter_funcs)


def main(args):
    if args.file is None or not os.path.exists(args.file):
        req = requests.get('http://atlas-agis-api.cern.ch/request/ddmendpoint/query/list/?json')
        assert req.status_code == 200
        content = req.content
        endpoints = json.loads(content)

    if args.file is not None:
        if not os.path.exists(args.file):
            with open(args.file, 'w') as f:
                cPickle.dump(endpoints, f)
        else:
            assert os.path.isfile(os.path.realpath(args.file))
            with open(args.file) as f:
                endpoints = cPickle.load(f)

    filter_funcs = build_filter_funcs(args)
    endpoints = (endpoint for endpoint in endpoints if match_all(filter_funcs, endpoint))

    # If any field is specified only show specified fields
    if args.show_fields:
        new_list = []
        for endpoint in endpoints:
            new_dict = {}
            new_list.append(new_dict)
            for key, val in endpoint.items():
                if key in args.show_fields:
                    new_dict[key] = val
        endpoints = new_list

    if args.pretty:
        pprint.pprint(list(endpoints))
    else:
        for endpoint in endpoints:
            fields = []
            for key, val in sorted(endpoint.items(), key=lambda t: tweak_priorities(t[0])):
                fields.append(str(val))

            print(','.join(fields))

if __name__ == '__main__':
    args = parse_args(sys.argv)
    main(args)
