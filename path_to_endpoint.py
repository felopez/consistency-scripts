#!/usr/bin/python
'''
Given the name of a RSE (possibly with shell like wildcards) and 
part of the path where the file is stored, the script prints
for each matching prefix:
    rse_name, spacetoken, url, prefix 
'''
import rucio.client
import os
import sys
import fnmatch
import operator
from itertools import imap as map
import warnings
import gfal



def format_warning(message, category, filename, lineno, file=None, line=None):
    return 'WARNING: {0}\n'.format(message)

warnings.formatwarning = format_warning

rses = (rse['rse'] for rse in rucio.client.rseclient.RSEClient().list_rses() if fnmatch.fnmatchcase(rse['rse'], sys.argv[1]))
try:
    path_fragment = sys.argv[2]
except:
    path_fragment = ''

for rse_name in rses:
    rse = rucio.client.rseclient.RSEClient().get_rse(rse_name)

    proto = filter(lambda prot: prot['scheme'] == 'srm', rse['protocols'])[0]

    hostname = proto['hostname']
    port = proto['port']
    prefix = proto['prefix']
    spacetoken = proto['extended_attributes']['space_token']
  
    if path_fragment.lower() not in prefix.lower():
        continue
    
    url = '{0}://{1}:{2}{3}'.format('srm', hostname, port, prefix)
    print('{0},{1},{2}'.format(rse_name, spacetoken, url, prefix)) 
