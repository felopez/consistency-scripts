#!/bin/sh

if [ $# -ne 1 ]; then
    echo "Usage: $0 [RSE|--all]" 1>&2
    exit 1
fi

opts=''
rse="$1"

if [ "$1" != '--all' ]; then
    opts="--rse $1"
fi

while read line; do
    did=$(echo $line | sed -r 's@^([^/]+)/.+/([^/]+)$@\1:\2@')

    echo "rucio list-file-replicas $opts \"$did\" | tail -n+3" 1>&2
    replicas=$(rucio list-file-replicas $opts "$did" | tail -n+3)


    if [ -n "$replicas" ]; then
        if [ -n "$(echo "$replicas" | grep -vF '???')" ]; then
            echo "REPLICA,$did,$(echo "$replicas" | tr '\n' ';' | tr -s '[[:blank:]]')"
        else
            echo "???,$did,$(echo "$replicas" | tr '\n' ';' | tr -s '[[:blank:]]')"
        fi
    else
        echo "NOTFOUND,$did,"
    fi
done
