#!/usr/bin/env python
import sys

# rucio/something/xx/yy/name
# rucio/user/something/xx/yy/name
# rucio/group/something/xx/yy/name

ignore_non_standard = '--ignore-non-standard' in sys.argv[1:]


for line in sys.stdin:
    line = line.strip()
    components = line.split('/')

    if len(components) >= 5:
        components = components[-5:]
    elif len(components) == 4:
        components.insert(0, 'rucio')
    elif not ignore_non_standard:
        raise Exception("Don't know how to translate {0}".format(line))



    if components[0] == 'rucio':
        print('{0}:{1}'.format(components[1], components[-1]))
    elif components[0] in ('user', 'group'):
        print('{0}.{1}:{2}'.format(components[0], components[1], components[-1]))
    elif not ignore_non_standard:
        raise Exception("Don't know how to translate {0}".format(line))
