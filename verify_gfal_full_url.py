#!/usr/bin/env python
import gfal
import itertools
import argparse

def chunks(iterable, size=1024):
    it = iter(iterable)
    chunk = list(itertools.islice(it, size))
    while chunk:
        yield chunk
        chunk = list(itertools.islice(it, size))


def exists(urls):
    status, internal, desc = gfal.gfal_init({'surls': urls})
    assert status == 0, 'gfal_init returned: {0}, "{1}"'.format(status, desc)
    gfal.gfal_ls(internal)
    status, internal, desc = gfal.gfal_get_results(internal)
    assert status == len(urls), 'gfal_get_results returned: {0}, "{1}"'.format(status, desc)
    return itertools.izip(urls, desc)

if __name__ == '__main__':
    import sys
    parser = argparse.ArgumentParser(
        description='Verify if the input URLs exists'
    )
    parser.add_argument('--bulk', type=int, default=1024, help='Number of paths sent to the server in each request')
    args = parser.parse_args()

    for paths in chunks(sys.stdin.xreadlines(), size=args.bulk):
        paths_status = exists(map(str.strip, paths))
        for url, status in paths_status:
            print('STATUS{0},{1}'.format(status['status'], url))
