#!/usr/bin/python
from functools import partial
from itertools import imap
from rucio.common.exception import SourceNotFound
from rucio.rse.protocols.gfal import Default
from rucio.rse import rsemanager
import argparse
import gfal2
import sys
import time
import errno
from datetime import datetime
import os
import stat

# Directories that may be on the <se>/<endpoint> others than "rucio"
SPECIAL_PATHS = ['SAM', 'dumps']
LS_FORMAT = '{0},{1},{2},{3},{4}\n'
RM_FORMAT = '{0},{1}\n'


def gfal_stat(gfal_ctx, path):
    try:
        print(path)
        return (True, gfal_ctx.stat(str(path)))
    except gfal2.GError as error:
        if error.code == errno.ENOENT or 'No such file' in error.message:
            return (False, None)
        raise



def formated_stat(stat_tuple, path):
    exists, st = stat_tuple
    if exists:
        formated = LS_FORMAT.format(
            'FOUND',
            path,
            st.st_size,
            'REGULAR' if stat.S_ISREG(st.st_mode) else 'UNKNOWN',
            datetime.fromtimestamp(st.st_mtime).isoformat(),
        )
    else:
        formated = LS_FORMAT.format(
            'NOTFOUND',
            path,
            None,
            None,
            None,
        )
    return formated


FULL_PREFIX_FORMAT = '{scheme}://{hostname}{optional_port}{web_service_path}/{prefix}/'
def full_prefix(proto):
    if proto.attributes['extended_attributes'] is not None:
        web_service_path = proto.attributes['extended_attributes'].get('web_service_path', '')
    else:
        web_service_path = ''

    return FULL_PREFIX_FORMAT.format(
        scheme=proto.attributes['scheme'],
        hostname=proto.attributes['hostname'],
        optional_port=':' + str(proto.attributes['port']) if proto.attributes['port'] else '',
        web_service_path=web_service_path,
        prefix='/'.join(proto.attributes['prefix'].strip('/').split('/')[:-1])
    )

def parse_input(path):
        path = path.strip().strip('/')
        if path == '':
            return path

        if not path.startswith('rucio/') and path.split('/')[0] not in SPECIAL_PATHS:
            # Prepend "rucio/" unless it is already there or it is either a "SAM/" or "dumps/" subpath.
            path = 'rucio/' + path

        return path

def process_input(gfal_ctx, read_proto, delete_proto, input_generator, full_urls, commands, output_files):

    read_full_prefix = full_prefix(read_proto)
    delete_full_prefix = full_prefix(delete_proto)

    for path in input_generator:
        if not full_urls:
            path = parse_input(path)
            if path == '':
                # Skip empty lines
                continue

        for command, output in zip(commands, output_files):
            if command == 'ls':
                if not full_urls:
                    ls_path = read_full_prefix + path
                output.write(formated_stat(gfal_stat(gfal_ctx, ls_path), ls_path))
            elif command == 'rm':
                if not full_urls:
                    rm_path = delete_full_prefix + path
                try:
                    delete_proto.delete(rm_path)
                except SourceNotFound:
                    output.write(RM_FORMAT.format('NOTFOUND', rm_path))
                else:
                    output.write(RM_FORMAT.format('DELETED', rm_path))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Verify if the input paths exists in any of the given DDM Endpoints'
    )
    parser.add_argument('--name-prefix', '-p', help='Prefix of the files where the results will be stored (it may include relative or absolute directories). It defaults to the name of the endpoint.', default=None)
    parser.add_argument('--endpoint', '-e', metavar='DDM_ENDPOINT', help='Each line in the input is a relative path inside the given endpoint or a deterministic physical name as defined in Rucio.', required=True)
    parser.add_argument('--full-urls', '-f', help='Each line in the input is a full URL of a file.', action='store_true')
    parser.add_argument('--bulk', '-b', type=int, default=1024, help='Number of paths sent to the server in each request.')
    parser.add_argument('commands', choices=['ls', 'rm'], nargs='+', help='Each command is applied to the input in order, the results are saved on <name-prefix>_<ls|rm>_<YYYYMMDD>_<HHMMSS>_<order>. If --endpoint is specified output-prefix defaults to its value.')
    args = parser.parse_args()

    rse_info = rsemanager.get_rse_info(args.endpoint)
    
    gfal_ctx = gfal2.creat_context()                                                 
    gfal_ctx.set_opt_string_list("SRM PLUGIN", "TURL_PROTOCOLS", ["gsiftp", "rfio", "gsidcap", "dcap", "kdcap"]) 
    
    read_proto = rsemanager.create_protocol(rse_info, 'read', scheme='srm')
    read_proto.connect()
    delete_proto = rsemanager.create_protocol(rse_info, 'delete', scheme='srm')
    delete_proto.connect()

    if args.name_prefix is None:
        args.name_prefix = args.endpoint

    output_files = []
    date = datetime.now()
    for order, command in enumerate(args.commands):
        filename = '{0}_{1}_{2}_{3}_{4}'.format(
            args.name_prefix,
            command,
            date.strftime('%Y%m%d'),
            date.strftime('%H%M%S'),
            order,
        )
        if os.path.exists(filename):
            raise Exception('Refusing to overwrite {0}'.format(filename))
        output_files.append(
            open(filename, 'w')
        )

    process_input(
        gfal_ctx,
        read_proto,
        delete_proto,
        sys.stdin.xreadlines(),
        args.full_urls,
        args.commands,
        output_files,
    )

    for output in output_files:
        output.close()

    read_proto.close()
    delete_proto.close()
