#!/usr/bin/env python
import json
import sys
import pprint
import operator

objects = json.loads(sys.stdin.read())

for i, attr in enumerate(sorted(objects[0].iterkeys())):
    if i == 0:
        sys.stdout.write(attr)
    else:
        sys.stdout.write(';{0}'.format(attr))
else:
    sys.stdout.write('\n')

for item in objects:
    for i, attr in enumerate(map(operator.itemgetter(1), sorted(item.iteritems(), key=operator.itemgetter(0)))):
        if i == 0:
            sys.stdout.write(attr)
        else:
            sys.stdout.write(';{0}'.format(attr))
    else:
        sys.stdout.write('\n')
